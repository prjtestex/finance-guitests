package ua.finance.guitests.model;

/**
 * Created by igor.korol on 27.02.2017.
 */
public enum SortingState {
    NONE("none"),
    ASCENDING("ascending"),
    DESCENDING("descending");
    private String value;
    SortingState(String value){
        this.value = value;
    }
    public String getSortingValue(){
        return value;
    }
}
