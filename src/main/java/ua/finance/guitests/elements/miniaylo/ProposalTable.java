package ua.finance.guitests.elements.miniaylo;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Block;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import ua.finance.guitests.model.SortingState;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@Block(@FindBy(css = ".proposal-table "))
public class ProposalTable extends HtmlElement {

    @FindBy(css = "th.c3")
    SortableColumnHeader currencyHeader;

    @FindBy(css = "tbody.on-data tr .c3>b")
    private List<WebElement> currencyRateItems;

    @FindBy(css = "tbody.on-data tr .c2>b")
    private List<WebElement> amountItems;

    public List<BigDecimal> getExchangeRateValues(){
        List<BigDecimal> values = currencyRateItems.stream().map(l->new BigDecimal(l.getText())).collect(Collectors.toList());
        return values;
    }
    @Override
    public Rectangle getRect() {
        return null;
    }

    @Override
    public <X> X getScreenshotAs(OutputType<X> outputType) throws WebDriverException {
        return null;
    }

    public void sortByCurrencyRate(SortingState sorting) {
        currencyHeader.sort(sorting);
    }

    public List<BigDecimal> getAmountValues() {
        List<BigDecimal> values = amountItems.stream().map(l->new BigDecimal(l.getText().replaceAll(" ", ""))).collect(Collectors.toList());
        return values;
    }
}
