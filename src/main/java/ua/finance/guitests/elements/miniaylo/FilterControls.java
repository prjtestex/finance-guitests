package ua.finance.guitests.elements.miniaylo;

import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Block;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import ru.yandex.qatools.htmlelements.element.TextInput;

import java.math.BigDecimal;

@Block(@FindBy(css = "[data-role='proposal-filter']"))
public class FilterControls extends HtmlElement {

    // AMOUNT
    @FindBy(id = "filter-amount-from")
    TextInput fromAmount;
    @FindBy(id = "filter-amount-to")
    TextInput toAmount;

    // RATE
    @FindBy(id = "filter-rate-from")
    TextInput fromCurrencyRate;
    @FindBy(id = "filter-rate-to")
    TextInput toCurrencyRate;

    @FindBy(css = ".btn-expand i")
    Button expandButton;

    @Override
    public Rectangle getRect() {
        return null;
    }

    @Override
    public <X> X getScreenshotAs(OutputType<X> outputType) throws WebDriverException {
        return null;
    }

    public void filterByAmount(BigDecimal from, BigDecimal to){
        if (!fromAmount.isDisplayed()){
            expandButton.click();
        }
        fromAmount.sendKeys(String.valueOf(from));
        toAmount.sendKeys(String.valueOf(to) + Keys.ENTER);
    }

    public void filterByCurrency(BigDecimal fromCurrency, BigDecimal toCurrency) {
        if (!fromCurrencyRate.isDisplayed()){
            expandButton.click();
        }
        fromCurrencyRate.sendKeys(String.valueOf(fromCurrency));
        toCurrencyRate.sendKeys(String.valueOf(toCurrency) + Keys.ENTER);
    }
}
