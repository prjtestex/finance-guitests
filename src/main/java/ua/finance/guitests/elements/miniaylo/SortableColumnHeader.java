package ua.finance.guitests.elements.miniaylo;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.WebDriverException;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import ua.finance.guitests.model.SortingState;

public class SortableColumnHeader extends HtmlElement {

    public SortingState getSortedState(){
        return SortingState.valueOf(this.getAttribute("aria-sort").toUpperCase());
    }

    public void sort(SortingState sortingState){
        for (SortingState s : SortingState.values()){
            if (sortingState.equals(getSortedState())){
                break;
            }else{
                this.click();
            }
        }
    }

    @Override
    public Rectangle getRect() {
        return null;
    }

    @Override
    public <X> X getScreenshotAs(OutputType<X> outputType) throws WebDriverException {
        return null;
    }
}
