package ua.finance.guitests.elements.miniaylo;

import org.openqa.selenium.WebDriver;
import ua.finance.guitests.model.SortingState;
import ua.finance.guitests.pages.BasePage;

import java.math.BigDecimal;
import java.util.List;

public class FinanceMiniayloPage extends BasePage {
    private ProposalTable table;
    private FilterControls filterControls;

    public FinanceMiniayloPage(WebDriver driver) {
        super(driver);
    }

    public void sortByCurrencyRate(SortingState sorting) {
        table.sortByCurrencyRate(sorting);
    }

    public List<BigDecimal> getCurrencyRateValues() {
        return table.getExchangeRateValues();
    }

    public List<BigDecimal> getAmountValues() {
        return table.getAmountValues();
    }

    public void filterByAmount(BigDecimal from, BigDecimal to){
        filterControls.filterByAmount(from, to);
    }

    public void filterByCurrency(BigDecimal fromCurrency, BigDecimal toCurrency) {
        filterControls.filterByCurrency(fromCurrency, toCurrency);
    }
}
