package ua.finance.guitests;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import ua.finance.guitests.elements.miniaylo.FinanceMiniayloPage;

import java.math.BigDecimal;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public abstract class BaseTest {

    protected WebDriver driver;
    protected FinanceMiniayloPage mainPage;
    protected abstract void preconditions();

    @Before
    public void loadStartPage() {
        driver = new FirefoxDriver();
        driver.get("http://miniaylo.finance.ua/");
        mainPage = new FinanceMiniayloPage(driver);
        preconditions();
    }

    protected void verifyListEquals(List<BigDecimal> actual, List<BigDecimal> expected){
        assertThat("List not equals", actual, equalTo(expected));
    }

    @After
    public void killWebDriver() {
        driver.quit();
    }
}
