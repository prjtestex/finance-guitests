package ua.finance.guitests.filter;

import org.junit.Test;
import java.math.BigDecimal;
import java.util.List;

public class FilterByCurrencyTest extends FilterBaseTest{

    @Override
    public List<BigDecimal> getNotFilteredItems() {
        return mainPage.getCurrencyRateValues();
    }

    @Test
    public void filterByCurrencyTest(){
        mainPage.filterByCurrency(fromValue, toValue);
        verifyColumnFiltered(expectedFilteredList, fromValue, toValue);
    }

}
