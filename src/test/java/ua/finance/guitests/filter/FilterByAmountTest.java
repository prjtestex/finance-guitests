package ua.finance.guitests.filter;

import org.junit.Test;
import java.math.BigDecimal;
import java.util.List;

public class FilterByAmountTest extends FilterBaseTest{

    @Override
    public List<BigDecimal> getNotFilteredItems() {
        return mainPage.getAmountValues();
    }

    @Test
    public void filterByAmountTest(){
        mainPage.filterByAmount(fromValue, toValue);
        verifyColumnFiltered(expectedFilteredList, fromValue, toValue);
    }
}
