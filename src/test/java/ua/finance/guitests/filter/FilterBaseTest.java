package ua.finance.guitests.filter;

import ua.finance.guitests.BaseTest;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static junit.framework.TestCase.assertTrue;

public abstract class FilterBaseTest extends BaseTest {

    public BigDecimal fromValue;
    public BigDecimal toValue;
    public List<BigDecimal> expectedFilteredList;
    public abstract List<BigDecimal> getNotFilteredItems();

    @Override
    public void preconditions() {
        List<BigDecimal> columnAllValues = getNotFilteredItems();
        Collections.sort(columnAllValues);

        int fromIndex = Math.round(columnAllValues.size()/2);
        int toIndex = columnAllValues.size()-1;

        expectedFilteredList = columnAllValues.subList(fromIndex, toIndex);
        Collections.sort(expectedFilteredList);

        fromValue = expectedFilteredList.get(0);
        toValue = expectedFilteredList.get(expectedFilteredList.size()-1);
    }

    protected void verifyColumnFiltered(List<BigDecimal> actual, BigDecimal from, BigDecimal to){
        List<BigDecimal> unexpectedItems = actual.stream()
                .filter(i->!((i.compareTo(from)==1||i.compareTo(from)==0)&&(i.compareTo(to)==-1 ||i.compareTo(to)==0)))
                .collect(Collectors.toList());
        assertTrue("Filtered list contains unexpected items : "+unexpectedItems.toString(), unexpectedItems.size()==0);
    }
}
