package ua.finance.guitests.sorting;

import org.junit.Test;
import ua.finance.guitests.BaseTest;
import ua.finance.guitests.model.SortingState;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SortByCurrencyTest extends BaseTest {

    @Test
    public void sortingByCurrencyAscendingTest() {
        sortingByCurrency(SortingState.ASCENDING);
        verifyListEquals(mainPage.getCurrencyRateValues(), getSortedList(SortingState.ASCENDING));
    }

    @Test
    public void sortingByCurrencyDescendingTest() {
        sortingByCurrency(SortingState.DESCENDING);
        verifyListEquals(mainPage.getCurrencyRateValues(), getSortedList(SortingState.DESCENDING));
    }

    private void sortingByCurrency(SortingState sortingState){
        mainPage.sortByCurrencyRate(sortingState);
    }

    private List<BigDecimal> getSortedList(SortingState sorting){
        List<BigDecimal> actualColumnValues = mainPage.getCurrencyRateValues();
        List<BigDecimal> expectedSorted = new ArrayList<>(actualColumnValues);
        if (sorting.equals(SortingState.ASCENDING)){
            Collections.sort(expectedSorted);
        }
        if (sorting.equals(SortingState.DESCENDING)){
            Collections.sort(expectedSorted, Collections.reverseOrder());
        }
        return expectedSorted;
    }

    @Override
    protected void preconditions() {
    }
}
